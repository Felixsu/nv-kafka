package co.ninjavan.experimental.kafka.config;

public enum Protos {

    /* Events */
    DRIVER_ACTION_PROTO("DRIVER_ACTION"),
    METADATA_PROTO("METADATA"),
    ORDER_EVENT_PROTO("ORDER_EVENT"),
    HUB_EVENT_PROTO("HUB_EVENT_PROTO"),
    ROUTE_EVENT_PROTO("ROUTE_EVENT"),
    SHIPPER_CREDENTIALS_UPDATED_EVENT_PROTO("SHIPPER_CREDENTIALS_UPDATED_EVENT"),
    UPDATE_DOUBLE_PROTO("UPDATE_DOUBLE"),

    /* Notifications */
    EMAIL_KAFKA_MESSAGE_PROTO("EMAIL_KAFKA_MESSAGE"),
    SMS_KAFKA_MESSAGE_PROTO("SMS_KAFKA_MESSAGE"),
    TEMPLATE_ATTACHMENT_PROTO("TEMPLATE_ATTACHMENT"),
    WEBHOOK_INTERNAL_PROTO("WEBHOOK_INTERNAL"),
    ORDER_STATUS("ORDER_STATUS"),

    INVALID_PROTO("INVALID"),

    ADD_TO_ROUTE_PROTO("ADD_TO_ROUTE"),
    PULL_OUT_OF_ROUTE_PROTO("PULL_OUT_OF_ROUTE");

    private String value;

    Protos(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Protos fromValue(String value) {
        if("driver_action".equalsIgnoreCase(value))
            return DRIVER_ACTION_PROTO;
        else if("metadata".equalsIgnoreCase(value))
            return METADATA_PROTO;
        else if("order_event".equalsIgnoreCase(value))
            return ORDER_EVENT_PROTO;
        else if("route_event".equalsIgnoreCase(value))
            return ROUTE_EVENT_PROTO;
        else if("shipper_credentials_updated_event".equalsIgnoreCase(value))
            return SHIPPER_CREDENTIALS_UPDATED_EVENT_PROTO;
        else if("update_double".equalsIgnoreCase(value))
            return UPDATE_DOUBLE_PROTO;
        else if("add_to_route".equalsIgnoreCase(value))
            return ADD_TO_ROUTE_PROTO;
        else if("pull_out_of_route".equalsIgnoreCase(value))
            return PULL_OUT_OF_ROUTE_PROTO;
        else if("webhook_internal".equalsIgnoreCase(value))
            return WEBHOOK_INTERNAL_PROTO;
        else if("email_kafka_message".equalsIgnoreCase(value))
            return EMAIL_KAFKA_MESSAGE_PROTO;
        else if("sms_kafka_message".equalsIgnoreCase(value))
            return SMS_KAFKA_MESSAGE_PROTO;
        else if("template_attachment".equalsIgnoreCase(value))
            return TEMPLATE_ATTACHMENT_PROTO;
        else
            return INVALID_PROTO;
    }
}
