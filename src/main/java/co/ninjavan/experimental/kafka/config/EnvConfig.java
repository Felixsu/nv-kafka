package co.ninjavan.experimental.kafka.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EnvConfig {

    //    KAFKA_BROKER_IP("KAFKA_BROKER_IP", "kafka-1.c.ninja-van-development.internal:9092, kafka-2.c.ninja-van-development.internal:9092, kafka-3.c.ninja-van-development.internal:9092"),
    KAFKA_BROKER_IP("KAFKA_BROKER_IP",
            "qa-global-kafka-statefulset-0.qa-global-kafka-service.qa.svc.cluster.local:9092," +
                    "qa-global-kafka-statefulset-1.qa-global-kafka-service.qa.svc.cluster.local:9092," +
                    "qa-global-kafka-statefulset-2.qa-global-kafka-service.qa.svc.cluster.local:9092"),
    KAFKA_CONSUMER_GROUP("KAFKA_CONSUMER_GROUP", "experimental.consumer-x"),
    KAFKA_ORDER_EVENTS_PROTO_TOPIC("KAFKA_ORDER_EVENTS_PROTO_TOPIC", "order-events-qa-proto-topic"),
    KAFKA_ORDERS_STATUS_PROTO_TOPIC("KAFKA_ORDERS_STATUS_PROTO_TOPIC", "orders-status-qa-proto-topic"),
    KAFKA_ORDER_CREATE_PROTO_TOPIC("KAFKA_ORDER_CREATE_PROTO_TOPIC", "order-create-qa-proto-topic"),
    KAFKA_DRIVER_JOB_PICKUP_ATTEMPTED_TOPIC("KAFKA_DRIVER_JOB_PICKUP_ATTEMPTED_TOPIC", "qa-driver-evt-job-pickup-attempted"),
    KAFKA_DRIVER_JOB_FAILED_TOPIC("KAFKA_DRIVER_JOB_FAILED_TOPIC", "qa-driver-evt-job-failed"),
    KAFKA_HUB_EVENTS_PROTO_TOPIC("KAFKA_HUB_EVENTS_PROTO_TOPIC", "hub-events-qa-proto-topic");

    final String key;
    final String value;

    EnvConfig(String key, String defaultValue) {
        final Logger LOGGER = LoggerFactory.getLogger(EnvConfig.class.getName());

        this.key = key;

        String envVal = System.getenv(key);
        if (envVal != null) {
            LOGGER.info("Key(" + key + ")  => " + envVal);
            value = envVal;
        } else {
            LOGGER.warn("!!! Key(" + key + ") => " + defaultValue + " (Default)");
            value = defaultValue;
        }
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "EnvConfig{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
