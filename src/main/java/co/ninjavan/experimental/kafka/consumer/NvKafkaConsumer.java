package co.ninjavan.experimental.kafka.consumer;

import co.ninjavan.experimental.kafka.config.EnvConfig;
import co.ninjavan.experimental.kafka.config.Protos;
import co.ninjavan.pojo.protos.kafka.order.status.OrderStatusProto;
import co.ninjavan.proto.core_v2.hub.HubEventProto;
import co.ninjavan.proto.events.DriverActionProto;
import co.ninjavan.proto.events.OrderEventProto;
import co.ninjavan.proto.notification.WebhookInternalProto;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import protos.kafka.notifications.email.EmailKafkaMessageProto;
import protos.kafka.notifications.sms.SmsKafkaMessageProto;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class NvKafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(NvKafkaConsumer.class.getName());

    private final KafkaConsumer<String, byte[]> consumer;

    public NvKafkaConsumer() {
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, EnvConfig.KAFKA_BROKER_IP.getValue());
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, EnvConfig.KAFKA_CONSUMER_GROUP.getValue());
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        properties.setProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        properties.setProperty(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");

        consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Arrays.asList(
                EnvConfig.KAFKA_ORDERS_STATUS_PROTO_TOPIC.getValue(),
            EnvConfig.KAFKA_ORDER_CREATE_PROTO_TOPIC.getValue()
                )
        );
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void listen() {
        int counter = 0;
        LOGGER.info("kafka consumer started on {}", ZonedDateTime.now());
        while (true) {
            ConsumerRecords<String, byte[]> records = consumer.poll(2000L);

            if (records.isEmpty()) {
                counter++;
                if (counter > 10) {
                    LOGGER.info("kafka queue empty");
                    counter = 0;
                }
                continue;
            } else {
                counter = 0;
            }
            for (ConsumerRecord<String, byte[]> record : records) {
                try {
                    String protoName = null;
                    int protoSize = record.value().length;
                    String proto = "";
                    int partition = record.partition();
                    long offset = record.offset();
                    String topic = record.topic();

                    String metadata = String.format("%s | PAR%d | OFF%d", topic, partition, offset);

                    switch (getProtoType(record.value())) {
                        case DRIVER_ACTION_PROTO: {
                            OrderEventProto.OrderEvent orderEvent = OrderEventProto.OrderEvent.parseFrom(record.value());
                            protoName = "DriverActionProto";
                            proto = orderEvent.toString();
                            break;
                        }
                        case ORDER_EVENT_PROTO: {
                            OrderEventProto.OrderEvent orderEvent = OrderEventProto.OrderEvent.parseFrom(record.value());
                            protoName = "OrderEventProto";
                            proto = orderEvent.toString();
                            break;
                        }
                        case WEBHOOK_INTERNAL_PROTO: {
                            WebhookInternalProto.WebhookInternal webhookInternal = WebhookInternalProto.WebhookInternal.parseFrom(record.value());
                            protoName = "WebhookInternalProto";
                            proto = webhookInternal.toString();
                            break;
                        }
                        case EMAIL_KAFKA_MESSAGE_PROTO: {
                            EmailKafkaMessageProto.EmailKafkaMessage emailKafkaMessage = EmailKafkaMessageProto.EmailKafkaMessage.parseFrom(record.value());
                            StringBuilder sb = new StringBuilder();
                            for (String email : emailKafkaMessage.getToEmailsList()) {
                                sb.append(String.format("[%s][%s][%s] %s \n", ZonedDateTime.now(), emailKafkaMessage.getSubject(), (emailKafkaMessage.getTemplate().isEmpty()) ? "No Template" : emailKafkaMessage.getTemplate(), email));
                            }

                            protoName = "EmailKafkaMessageProto";
                            proto = sb.append('\n').append(emailKafkaMessage.toString()).toString();
                            break;
                        }
                        case TEMPLATE_ATTACHMENT_PROTO: {
                            EmailKafkaMessageProto.EmailKafkaMessage.TemplateAttachment templateAttachment = EmailKafkaMessageProto.EmailKafkaMessage.TemplateAttachment.parseFrom(record.value());

                            protoName = "TemplateAttachmentProto";
                            proto = templateAttachment.toString();
                            break;
                        }
                        case SMS_KAFKA_MESSAGE_PROTO: {
                            SmsKafkaMessageProto.SmsKafkaMessage smsKafkaMessage = SmsKafkaMessageProto.SmsKafkaMessage.parseFrom(record.value());

                            protoName = "SmsKafkaMessageProto";
                            proto = smsKafkaMessage.toString();
                            break;
                        }
                        case ORDER_STATUS: {
                            OrderStatusProto.OrderStatus orderStatus = OrderStatusProto.OrderStatus.parseFrom(record.value());
                            protoName = "OrderStatusProto";
                            proto = orderStatus.toString();
                            break;
                        }
                        case HUB_EVENT_PROTO: {
                            HubEventProto.HubEvent hubEvent = HubEventProto.HubEvent.parseFrom(record.value());
                            protoName = "HubEventStatusProto";
                            proto = hubEvent.toString();
                            break;
                        }
                    }
                    String message;
                    if (protoName != null) {
                        message = String.format("\n[SUCCESS][%s][%s][%db]\n\n%s\n", metadata, protoName, protoSize, proto);
                    } else {
                        message = String.format("\n[FAILURE][%s][Unknown][%db]\n\n", metadata, protoSize);
                    }
                    LOGGER.info(message);
                } catch (InvalidProtocolBufferException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }

    }

    private Protos getProtoType(byte[] value) {
        if (isTemplateAttachment(value)) {
            return Protos.EMAIL_KAFKA_MESSAGE_PROTO;
        } else if (isSmsKafkaMessage(value)) {
            return Protos.SMS_KAFKA_MESSAGE_PROTO;
        } else if (isEmailKafkaMessage(value)) {
            return Protos.EMAIL_KAFKA_MESSAGE_PROTO;
        } else if (isWebhookInternal(value)) {
            return Protos.WEBHOOK_INTERNAL_PROTO;
        } else if (isDriverAction(value)) {
            return Protos.DRIVER_ACTION_PROTO;
        } else if (isOrderEvent(value)) {
            return Protos.ORDER_EVENT_PROTO;
        } else if (isOrderStatusEvent(value)) {
            return Protos.ORDER_STATUS;
        } else if (isHubEvent(value)) {
            return Protos.ORDER_STATUS;
        } else {
            return Protos.INVALID_PROTO;
        }
    }

    private boolean isTemplateAttachment(byte[] value) {
        try {
            return !EmailKafkaMessageProto.EmailKafkaMessage.TemplateAttachment.parseFrom(value).getContent().isEmpty();
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isEmailKafkaMessage(byte[] value) {
        try {
            return EmailKafkaMessageProto.EmailKafkaMessage.parseFrom(value).getToEmailsCount() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isSmsKafkaMessage(byte[] value) {
        try {
            return SmsKafkaMessageProto.SmsKafkaMessage.parseFrom(value).getNumber().length() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isWebhookInternal(byte[] value) {
        try {
            return WebhookInternalProto.WebhookInternal.parseFrom(value).getTopic().compareTo("dev-notifications-evt-webhook-internal") == 0;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isDriverAction(byte[] value) {
        List<DriverActionProto.DriverAction.EventCase> events = Arrays.asList(
                DriverActionProto.DriverAction.EventCase.JOB,
                DriverActionProto.DriverAction.EventCase.START_ROUTE
        );
        try {
            DriverActionProto.DriverAction driverAction = DriverActionProto.DriverAction.parseFrom(value);
            return events.contains(driverAction.getEventCase());
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isOrderEvent(byte[] value) {
        List<OrderEventProto.OrderEvent.EventCase> events = Arrays.asList(
                OrderEventProto.OrderEvent.EventCase.CANCEL,
                OrderEventProto.OrderEvent.EventCase.FORCED_SUCCESS,
                OrderEventProto.OrderEvent.EventCase.RESCHEDULE,
                OrderEventProto.OrderEvent.EventCase.RESUME,
                OrderEventProto.OrderEvent.EventCase.RTS,
                OrderEventProto.OrderEvent.EventCase.PRICING_CHANGE,
                OrderEventProto.OrderEvent.EventCase.ADDRESS_VERIFICATION,
                OrderEventProto.OrderEvent.EventCase.SHIPMENT_COMPLETED,
                OrderEventProto.OrderEvent.EventCase.REMOVED_FROM_SHIPMENT,
                OrderEventProto.OrderEvent.EventCase.ADDED_TO_SHIPMENT,
                OrderEventProto.OrderEvent.EventCase.UPDATE_ADDRESS,
                OrderEventProto.OrderEvent.EventCase.UPDATE_CASH,
                OrderEventProto.OrderEvent.EventCase.UPDATE_CONTACT,
                OrderEventProto.OrderEvent.EventCase.UPDATE_INSTRUCTION,
                OrderEventProto.OrderEvent.EventCase.UPDATE_DIMENSION,
                OrderEventProto.OrderEvent.EventCase.UPDATE_DISTRIBUTION_POINT,
                OrderEventProto.OrderEvent.EventCase.UPDATE_SLA,
                OrderEventProto.OrderEvent.EventCase.UPDATE_JOB_INFO,
                OrderEventProto.OrderEvent.EventCase.ASSIGNED_TO_DP,
                OrderEventProto.OrderEvent.EventCase.FROM_SHIPPER_TO_DP,
                OrderEventProto.OrderEvent.EventCase.FROM_DP_TO_CUSTOMER,
                OrderEventProto.OrderEvent.EventCase.FROM_DRIVER_TO_DP,
                OrderEventProto.OrderEvent.EventCase.FROM_DP_TO_DRIVER,
                OrderEventProto.OrderEvent.EventCase.DRIVER_INBOUND_SCAN,
                OrderEventProto.OrderEvent.EventCase.DRIVER_PICKUP_SCAN,
                OrderEventProto.OrderEvent.EventCase.HUB_INBOUND_SCAN,
                OrderEventProto.OrderEvent.EventCase.PARCEL_ROUTING_SCAN,
                OrderEventProto.OrderEvent.EventCase.ROUTE_INBOUND_SCAN,
                OrderEventProto.OrderEvent.EventCase.ROUTE_TRANSFER_SCAN,
                OrderEventProto.OrderEvent.EventCase.FORCED_FAILURE,
                OrderEventProto.OrderEvent.EventCase.OUTBOUND_SCAN,
                OrderEventProto.OrderEvent.EventCase.REVERT_COMPLETED,
                OrderEventProto.OrderEvent.EventCase.UNASSIGNED_FROM_DP,
                OrderEventProto.OrderEvent.EventCase.TRANSFERRED_TO_THIRD_PARTY,
                OrderEventProto.OrderEvent.EventCase.TICKET_CREATED,
                OrderEventProto.OrderEvent.EventCase.TICKET_UPDATED,
                OrderEventProto.OrderEvent.EventCase.TICKET_RESOLVED,
                OrderEventProto.OrderEvent.EventCase.UPDATE_INSURANCE,
                OrderEventProto.OrderEvent.EventCase.ADD_TO_ROUTE,
                OrderEventProto.OrderEvent.EventCase.PULL_OUT_OF_ROUTE,
                OrderEventProto.OrderEvent.EventCase.REJECTED,
                OrderEventProto.OrderEvent.EventCase.UNSET_ASIDE,
                OrderEventProto.OrderEvent.EventCase.UPDATE_TAGS,
                OrderEventProto.OrderEvent.EventCase.DELETED,
                OrderEventProto.OrderEvent.EventCase.UPDATED_AT_DP
        );
        try {
            OrderEventProto.OrderEvent orderEvent = OrderEventProto.OrderEvent.parseFrom(value);
            return events.contains(orderEvent.getEventCase());
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isOrderStatusEvent(byte[] value) {
        try {
            OrderStatusProto.OrderStatus message = OrderStatusProto.OrderStatus.parseFrom(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isHubEvent(byte[] value) {
        try {
            HubEventProto.HubEvent message = HubEventProto.HubEvent.parseFrom(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
