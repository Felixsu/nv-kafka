package co.ninjavan.experimental.kafka;

import co.ninjavan.experimental.kafka.config.EnvConfig;
import co.ninjavan.experimental.kafka.consumer.NvKafkaConsumer;

public class App {

    public static void main(String[] args) {
        NvKafkaConsumer nvKafkaConsumer = new NvKafkaConsumer();

        nvKafkaConsumer.listen();


    }
}
