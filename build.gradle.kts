import com.google.protobuf.gradle.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    idea
    application
    kotlin("jvm") version "1.3.41"
    id("com.google.protobuf") version "0.8.10"
}

group = "co.ninjavan"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven {
        url = uri("https://nexus.ninjavan.co/repository/maven-releases/")
        credentials {
            username = "ninjavan"
            password = "Eiquoo3Vsheda6SheiQu5Wio"
        }
    }
}

application {
    mainClassName = "co.ninjavan.experimental.kafka.App"
}

sourceSets {
    main {
        proto {
            // In addition to the default 'src/main/proto'
            srcDir("src/main/protobuf")
        }
        java {
            srcDir("generated/main/java")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.6.1"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:1.15.1"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                // Apply the "grpc" plugin whose spec is defined above, without options.
                id("grpc")
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/gen"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.bouncycastle:bcprov-jdk16:1.46")
    implementation("org.apache.commons:commons-lang3:3.0")
    implementation("org.apache.commons:commons-text:1.6")
    implementation("commons-codec:commons-codec:1.11")

    implementation("com.fasterxml.uuid:java-uuid-generator:3.1.5")

    implementation("com.hazelcast:hazelcast:3.12.1")
    implementation("com.hazelcast:hazelcast-client:3.12.1")

    implementation("co.ninjavan:kafka-commons_2.11:3.9.0")
    implementation("co.ninjavan:notifications-utilities_2.11:3.16.0")
    implementation("co.ninjavan:api-client-notifications_2.11:3.1.0")
    implementation("co.ninjavan:service-commons_2.11:4.3.1")

    implementation("com.google.protobuf:protobuf-java:3.6.1")
    implementation("io.grpc:grpc-stub:1.15.1")
    implementation("io.grpc:grpc-protobuf:1.15.1")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.9.8")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.9.8")
    implementation("com.fasterxml.jackson.module:jackson-module-parameter-names:2.9.8")

    implementation("ch.qos.logback:logback-core:1.2.3")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation ("org.slf4j:slf4j-api:1.7.26")

    testCompile("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}